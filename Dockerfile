FROM openjdk:8-jre-alpine
COPY /target/investimento-*jar ./investimento.jar
CMD ["/usr/bin/java", "-jar", "investimento.jar"]