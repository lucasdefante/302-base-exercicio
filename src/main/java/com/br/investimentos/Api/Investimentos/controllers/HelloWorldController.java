package com.br.investimentos.Api.Investimentos.controllers;

import com.br.investimentos.Api.Investimentos.services.InformacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/helloworld")
public class HelloWorldController {

    @Autowired
    private InformacaoService informacaoService;

    @GetMapping
    public String printHelloWorld() {
        return "Hello, World! Id do Pod conectado: " + informacaoService.retrieveInstanceInfo();
    }

}
